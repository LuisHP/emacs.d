;; -*- lexical-binding: t; -*-

;; The primary goal of this file is to tangle [[file:main.org][main.org]] if it hasn't been
;; tangled already.

;; * Startup Optimizations

;; ** GC cons threshold

;; A big contributor to long startup times is the garbage collector. When
;; performing a large number of calculations, it can make a big difference to
;; increase the [[helpvar:gc-cons-threshold][gc-cons-threshold]], or the /number of bytes of consing between
;; garbage collections/. If garbage collection is done too often between
;; calculations it will affect the time they take. Besides, the default value is
;; usually too low for modern machines.

(defvar void:gc-cons-threshold-max (eval-when-compile (* 256 1024 1024))
  "The upper limit for `gc-cons-threshold'.

When VOID is performing computationally intensive operations,
`gc-cons-threshold' is set to this value.")

(defvar void:gc-cons-threshold (eval-when-compile (* 16 1024 1024))
  "The default value for `gc-cons-threshold'.

If you experience freezing, decrease this. If you experience stuttering,
increase this.")

(defvar void:gc-cons-percentage-max 0.6
  "The upper limit for `gc-cons-percentage'.

When VOID is performing computationally intensive operations,
`gc-cons-percentage' is set to this value.")

(defvar void:gc-cons-percentage 0.1
  "The default value for `gc-cons-percentage'.")

;; ** Boost gc threshold

(setq gc-cons-threshold void:gc-cons-threshold-max
      gc-cons-percentage void:gc-cons-percentage-max)

;; ** Check Version

(eval-when-compile
  (and (version< emacs-version "26.1")
       (error "Detected Emacs %s. VOID only supports Emacs 26.1 and higher"
              emacs-version)))

;; ** Built-in Libraries

;; *** cl

;; The =cl library= is used by many packages. For many the main reason to
;; require it is to use =cl-loop=. I don't really like =cl-loop= but even still
;; the cl library has a lot of useful functions and macros such as =letf= and =cl-find=.

(eval-when-compile (require 'cl-lib))

;; *** subr-x

(eval-when-compile (require 'subr-x))

;; *** rx

(eval-when-compile (require 'rx))

;; * Debugging

;; Proponents of literate configs often tout Org's readability and organization,
;; but rarely do they discuss what I think has a lot of potential: tangling. As
;; opposed viewing tangling as something that needs to be done as a cost of having
;; a literate configuration, I use it as a means to make my code error resistant
;; and debugging friendly. Exploiting the fact that my code is separated into
;; sections by category, I wrap each section in the [[helpfn:elisp-block!][elisp-block!]] macro during
;; tangling. For the most part, =elisp-block!= is a wrapper around [[helpfn:condition-case][condition-case]],
;; a macro that catches any errors and lets you do what you want with it. As it
;; stands, when any kind of error occurs in [[file:main.el][main.el]] instead of just crashing (like most
;; other emacs configs) the error is caught, and a message is sent to the
;; =*messages*= buffer specifying what kind of error it is and the path in
;; [[file:main.org][main.org]] which triggered the error. In a non-literate config, perhaps the most
;; annoying consequence of this would be having to litter your init files with
;; =elisp-block!= boilterplate. However with a literate config, you can have your
;; cake and eat it too; you can get the benefit of =elisp-block!= without it
;; actually littering the code you actually look at (in your org file).

;; ** Debug Mode

;; Variable that's used to check whether VOID is currently being debugged or
;; not. When this variable is active, VOID displays lots of messages describing
;; what it's doing.

(defvar void:debug-mode (or (getenv "DEBUG") init-file-debug)
  "If non-nil, enable VOID debugging features.

VOID is in `void:debug-mode' when it was started with the `--debug-init' flag.
Such features include more messaging parts of VOID's startup.")

;; ** Init Errors

;; It is likely, I will want to see more detailed information about any errors I
;; get messaged about Void's initialization. Therefore, I store them in
;; [[helpvar:void:init-errors][void-init-errors]].

(defvar void:init-errors nil
  "List of errors that occurred during VOID initialization.")

;; ** Logging Messages

(defmacro log! (format-string &rest args)
  "Log to *Messages* if `void:debug-mode' is on.

This macro does not interrupt the minibuffer if it is in use, but still logs to
*Messages*. It accepts the same arguments as `message'."
  (when void:debug-mode
    `(let ((inhibit-message (active-minibuffer-window)))
       (message
        ,(concat (propertize "VOID " 'face 'font-lock-comment-face)
                 format-string)
        ,@args))))


;; * Tangling

;; ** Check if I need to tangle

;; Laziness is a strength in programming. I only want to tangle
;; =void:main-org-file= when actually need to. Tangling is done at the expense of
;; some (small) initialization time increase. If it can be avoided, all the better.

(defun void:needs-tangling-p ()
  "Whether `void:main-org-file' needs to be tangled.

Tangling needs to occur when either `void:main-elisp-file' does not exist or
`void:main-org-file' is newer than `void:main-elisp-file'."
  (or (not (file-exists-p void:main-elisp-file))
      (file-newer-than-file-p void:main-org-file void:main-elisp-file)))

;; ** Files relevant to tangling

(defvar void:main-org-file (concat user-emacs-directory "main.org")
  "Org file containing most of VOID's initialization code.")

(defvar void:main-elisp-file (concat user-emacs-directory "main.el")
  "The elisp file that `void:main-org-file' tangles to.")

;; ** Helpers for Tangle Function

;; *** Alias =match-string-np=

(defalias 'match-string-np 'match-string-no-properties)

;; ** Regexps

;; *** Source Block

;; I got this regexp from the value of [[helpvar:org-src-block-regxp][org-babel-src-block-regexp]]. I can't just use
;; =org-babel-src-block-regexp= because I'd have to load part of org babel to use
;; it. So here I make my own. I got this =rx= expression by calling
;; [[helpfn:rxt-convert-to-rx][rxt-convert-to-rx]] from [[github:joddie/pcre2el][pcre2el]] on the raw emacs regular expression.

(defvar org-babel-src-block-regexp
  (concat
   ;; (1) indentation                 (2) lang
   "^\\([ \t]*\\)#\\+begin_src[ \t]+\\([^ \f\t\n\r\v]+\\)[ \t]*"
   ;; (3) switches
   "\\([^\":\n]*\"[^\"\n*]*\"[^\":\n]*\\|[^\":\n]*\\)"
   ;; (4) header arguments
   "\\([^\n]*\\)\n"
   ;; (5) body
   "\\([^\000]*?\n\\)??[ \t]*#\\+end_src")
  "Regexp used to identify code blocks.")

;; ** Tangle Function

;; I concat =void:heading-regexp= and =void:src-block-regexp= so that I can make
;; the search in one pass. However, in doing this, the match group numbers are not
;; the same for =void:src-block-regexp= so I make sure to account for that.

;; Huge "gotcha"! [[helpfn:string-trim-right][string-trim-right]] modifies its match data. It really should
;; mention that in its documentation. Even better, it should just use
;; [[helpfn:string-match-p][string-match-p]] instead of [[helpfn:string-match][string-match]] because the former does not modify the [[info:elisp#Match
;;  Data][match-data]].

(defun void:tangle-org-file ()
  "Tangle `void:main-org-file'."
  (let* ((title ";; -*- lexical-binding: t; -*-")
         (case-fold-search nil)
         (count 0) (lang) (code) (body))
    (with-temp-buffer
      (insert-file-contents-literally void:main-org-file)
      (goto-char (point-min))
      (while (re-search-forward org-babel-src-block-regexp nil t nil)
        (setq lang (match-string-no-properties 2))
        (setq body (match-string-no-properties 5))
        (when (and (string= lang "emacs-lisp") body)
          (cl-incf count)
          (setq code (concat code "\n" body))))
      (setq code (concat title "\n" code))
      (message "Tangled %d source blocks" count)
      ;; Write `void:main-elisp-file'.
      (let ((file-coding-system-alist nil)
            (coding-system-for-write 'binary))
        (with-temp-file void:main-elisp-file
          (setq buffer-file-coding-system 'binary)
          (set-buffer-multibyte nil)
          (insert code))))))

;; ** Conditionally Tangle =void:main-org-file=

(when (void:needs-tangling-p)
  (log! "Tangling... main.org -> main.el")
  (void:tangle-org-file))

;; * Main

;; ** Load Main

;; The [[helpvar:file-name-handler-alist][file-name-handler-alist]] is consulted on every call to ~load~ or ~require~.
;; Here I save it's value so that when I set it to ~nil~ I'll get a minor
;; performance boost.

(let (file-name-handler-alist)
  (load (concat user-emacs-directory "main") :noerror :nomessage))


;; ** Restore GC after delay

;; Restore gc cons threshold (after a 3 second delay). I got this from [[https://github.com/hlissner/doom-emacs/blob/develop/core/core.el#L283][doom]].
;; Instead of just doing this immediately, we give it 3 extra seconds because
;; packages are still being incrementally loaded (see [[file:main.org::*Load Incrementally][incremental loading]]).

(run-with-idle-timer
 7 nil (lambda ()
         (setq gc-cons-threshold void:gc-cons-threshold
               gc-cons-percentage void:gc-cons-percentage)))

;; ** Garbage collecting when focused out

;; I got this from [[reddit:bqu69o/making_emacs_snappier_i_need_a_second_opinion/][reddit post]] on making emacs snappier and from checking out
;; [[https://github.com/hlissner/doom-emacs/blob/develop/init.el][doom's init file]].

(add-hook 'focus-out-hook #'garbage-collect)
